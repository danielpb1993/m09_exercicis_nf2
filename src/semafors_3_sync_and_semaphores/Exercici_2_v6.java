package semafors_3_sync_and_semaphores;

import java.util.ArrayList;

public class Exercici_2_v6 {
    static public void inicialitzarPrograma() {
        int numTubs = 3;
        SistemaDeGuiaDeTorpedes sistemaDeGuiaDeTorpedes = new SistemaDeGuiaDeTorpedes(numTubs);
        int numTorpedes = 10;
        Thread[] torpedes = new Thread[numTorpedes];

        sistemaDeGuiaDeTorpedes.imprimirUsDelSistemaDeGuia("Exercici_7.inicialitzarPrograma() - INICI");

        for (int i = 0; i < numTorpedes; i++){
            Torpede torpedeTmp = new Torpede(sistemaDeGuiaDeTorpedes);

            torpedes[i] = new Thread(torpedeTmp);
            torpedes[i].setName("torpede " + i);

            torpedes[i].start();
        }

        try {
            torpedes[numTorpedes - 1].join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        sistemaDeGuiaDeTorpedes.imprimirUsDelSistemaDeGuia("Exercici_7.inicialitzarPrograma() - FI");
    }
}
