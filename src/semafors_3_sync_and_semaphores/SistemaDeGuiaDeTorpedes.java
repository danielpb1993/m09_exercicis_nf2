package semafors_3_sync_and_semaphores;

import java.util.concurrent.Semaphore;

public class SistemaDeGuiaDeTorpedes {
    private int numDeTorpedesQuePotGuiarSimultaneament;
    private Semaphore semaforAssignacioSistemaDeGuia;
    private boolean[] usDelSistemaDeGuia;   			// Array que controla l'us del sistema de guia. True = ocupat.
    private Semaphore semaforPerEscriureEnUsDelSistemaDeGuia;

    public SistemaDeGuiaDeTorpedes(int num) {
        numDeTorpedesQuePotGuiarSimultaneament = num;
        semaforAssignacioSistemaDeGuia = new Semaphore(num);
        usDelSistemaDeGuia = new boolean[num];
        semaforPerEscriureEnUsDelSistemaDeGuia = new Semaphore(1);
    }

    public void imprimirUsDelSistemaDeGuia(String nomThread) {
        String resultat = "";
        boolean primeraVegada = true;
        for (int i = 0; i < usDelSistemaDeGuia.length; i++) {
            if (primeraVegada == true) {
                resultat = nomThread + ", usDelSistemaDeGuia = [" + usDelSistemaDeGuia[i];

                primeraVegada = false;
            } else {
                resultat = resultat + ", " + usDelSistemaDeGuia[i];
            }
        }

        resultat = resultat + "]";

        System.out.println(resultat);
    }

    public int assignarSistemaDeGuia(String nomThread) {
        int i = 0;

        try {
            semaforPerEscriureEnUsDelSistemaDeGuia.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        while (i < numDeTorpedesQuePotGuiarSimultaneament) {
            if (usDelSistemaDeGuia[i] == false) {
                usDelSistemaDeGuia[i] = true;

                break;
            }

            i++;
        }
        System.out.println(nomThread + ", numUsDelSistemaDeGuia = " + i);

        imprimirUsDelSistemaDeGuia(nomThread);

        semaforPerEscriureEnUsDelSistemaDeGuia.release();

        return (i);
    }

    public int adquirirSistemaDeGuia(String nomThread) {
        try {
            semaforAssignacioSistemaDeGuia.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return(assignarSistemaDeGuia(nomThread));
    }

    public void alliberarSistemaDeGuia(int numUsDelSistemaDeGuia, String nomThread) {
        try {
            semaforPerEscriureEnUsDelSistemaDeGuia.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        usDelSistemaDeGuia[numUsDelSistemaDeGuia] = false;

        imprimirUsDelSistemaDeGuia(nomThread);

        semaforPerEscriureEnUsDelSistemaDeGuia.release();

        semaforAssignacioSistemaDeGuia.release();
    }

}
