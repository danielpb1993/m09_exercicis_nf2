package semafors_2_only_semaphores;

public class DeptCienciaProductor_v5 implements Runnable{
    private MagatzemCombustible_v5 magatzemCombustible_v5;

    public DeptCienciaProductor_v5(MagatzemCombustible_v5 magatzemCombustible_v5) {
        this.magatzemCombustible_v5 = magatzemCombustible_v5;
    }
    @Override
    public void run() {
        int i;
        boolean exitOperacio;

        System.out.println("1111 - DeptCienciaProductor.INICI");

        i = 0;
        while (i < 20) {
            exitOperacio = magatzemCombustible_v5.produirContenidorDeCombustible();

            if (exitOperacio == true) {
                i++;
            }
        }
        System.out.println("1111 - DeptCienciaProductor.FI");
    }
}
