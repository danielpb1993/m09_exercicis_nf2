package semafors_2_only_semaphores;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Semaphore;

public class MagatzemCombustible_v5 {
    ArrayList<Character> posicionsEnMagatzem = new ArrayList<>();
    private Semaphore semaforAccesALListaDePosicions;

    public MagatzemCombustible_v5() {
        Character[] posicions = {'0','0','0','0','0','0','0','0','0','0'};
        posicionsEnMagatzem.addAll(Arrays.asList(posicions));
        semaforAccesALListaDePosicions = new Semaphore(1);
    }

    public int numContenidorsAlMAgatzem() {
        int numContenidors = 0;
        for (char ocupada : posicionsEnMagatzem) {
            if (ocupada == '1') {
                numContenidors++;
            }
        }
        return numContenidors;
    }

    public boolean produirContenidorDeCombustible() {
        int posTmp;
        boolean exitOperacio = false;
        try {
            semaforAccesALListaDePosicions.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("1111.1 - DeptCienciaProductor, numContenidorsAlMagatzem() = " + numContenidorsAlMAgatzem() + ", " + posicionsEnMagatzem);
        if (numContenidorsAlMAgatzem() < 10) {
            posTmp = posicionsEnMagatzem.indexOf('0');
            posicionsEnMagatzem.set(posTmp, '1');
            exitOperacio = true;
        } else {
            exitOperacio = false;
        }
        System.out.println("1111.2 - DeptCienciaProductor, numContenidorsAlMagatzem() = " + numContenidorsAlMAgatzem() + ", " + posicionsEnMagatzem + ", exitOperacio = " + exitOperacio);
        semaforAccesALListaDePosicions.release();
        return exitOperacio;
    }

    public boolean consumirContenidorDeCombustible() {
        int posTmp;
        boolean exitOperacio = false;
        try {
            semaforAccesALListaDePosicions.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("2222.1 - " + Thread.currentThread().getName() + ", numContenidorsAlMagatzem() = " + numContenidorsAlMAgatzem() + ", " + posicionsEnMagatzem);
        if (numContenidorsAlMAgatzem() > 0) {

            posTmp = posicionsEnMagatzem.indexOf('1');
            posicionsEnMagatzem.set(posTmp, '0');
            exitOperacio = true;
        } else {
            exitOperacio = false;
        }
        System.out.println("2222.2 - " + Thread.currentThread().getName() + ", numContenidorsAlMagatzem() = " + numContenidorsAlMAgatzem() + ", " + posicionsEnMagatzem + ", exitOperacio = " + exitOperacio);
        semaforAccesALListaDePosicions.release();
        return exitOperacio;
    }
}
