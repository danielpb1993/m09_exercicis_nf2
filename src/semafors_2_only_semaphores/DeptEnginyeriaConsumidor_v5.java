package semafors_2_only_semaphores;

public class DeptEnginyeriaConsumidor_v5 implements Runnable {
    MagatzemCombustible_v5 magatzemCombustible_v5;
    private int numContenidorsPerAgafar;

    public DeptEnginyeriaConsumidor_v5(MagatzemCombustible_v5 magatzemCombustible_v5, int num) {
        this.magatzemCombustible_v5 = magatzemCombustible_v5;
        this.numContenidorsPerAgafar = num;
    }

    @Override
    public void run() {
        int i = 0;
        boolean exitOperacio;
        System.out.println("2222 - " + Thread.currentThread().getName() + ".INICI");

        while (i < numContenidorsPerAgafar) {
            exitOperacio = magatzemCombustible_v5.consumirContenidorDeCombustible();
            if (exitOperacio == true) {
                i++;
            }
        }
        System.out.println("2222 - " + Thread.currentThread().getName() + ".FI");
    }
}
