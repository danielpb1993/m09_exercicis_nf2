package semafors_2_only_semaphores;

public class Exercici_2_v5 {
    static public void inicialitzarPrograma() {
        MagatzemCombustible_v5 magatzemCombustible_v5 = new MagatzemCombustible_v5();
        DeptCienciaProductor_v5 deptCienciaProductor_v5 = new DeptCienciaProductor_v5(magatzemCombustible_v5);
        DeptEnginyeriaConsumidor_v5 deptEnginyeriaConsumidor_v5 = new DeptEnginyeriaConsumidor_v5(magatzemCombustible_v5, 7);
        DeptEnginyeriaConsumidor_v5 deptEnginyeriaConsumidor_v5_2 = new DeptEnginyeriaConsumidor_v5(magatzemCombustible_v5, 10);

        System.out.println("Exercici_6.inicialitzarPrograma() - INICI");
        System.out.println("Exercici_6.magatzemDeCombustible.posicionsEnMagatzem = " + magatzemCombustible_v5.posicionsEnMagatzem);

        Thread deptCienciaProductor = new Thread(deptCienciaProductor_v5);
        Thread deptEnginyeriaConsumidor = new Thread(deptEnginyeriaConsumidor_v5);
        deptEnginyeriaConsumidor.setName("deptEnginyeriaConsumidor_1");

        Thread deptEnginyeriaConsumidor2 = new Thread(deptEnginyeriaConsumidor_v5_2);
        deptEnginyeriaConsumidor2.setName("deptEnginyeriaConsumidor_2");


        deptCienciaProductor.start();
        deptEnginyeriaConsumidor.start();
        deptEnginyeriaConsumidor2.start();

        try {
            deptCienciaProductor.join();
            deptEnginyeriaConsumidor.join();
            deptEnginyeriaConsumidor2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Exercici_6.magatzemDeCombustible.posicionsEnMagatzem = " + magatzemCombustible_v5.posicionsEnMagatzem);
        System.out.println("Exercici_6.inicialitzarPrograma() - FI");
    }
}
