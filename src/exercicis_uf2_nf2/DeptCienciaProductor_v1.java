package exercicis_uf2_nf2;

public class DeptCienciaProductor_v1 extends Thread {
    private MagatzemCombustible_v1 magatzemCombustible_v1;

    public DeptCienciaProductor_v1(MagatzemCombustible_v1 magatzemCombustible_v1) {
        this.magatzemCombustible_v1 = magatzemCombustible_v1;
    }

    @Override
    public void run() {
        int i;

        System.out.println("1111 - DeptCienciaProductor.INICI");

        i = 0;
        while (i < 20) {
            if (magatzemCombustible_v1.numContenidorsAlMagatzem() < 10) {
                System.out.println("1111.1 - DeptCienciaProductor.run(), numContenidorsAlMagatzem() = " + magatzemCombustible_v1.numContenidorsAlMagatzem() +  ", " + new String(magatzemCombustible_v1.posicionsEnMagatzem));
                magatzemCombustible_v1.produirContenidorDeCombustible();
                System.out.println("1111.2 - DeptCienciaProductor.run(), numContenidorsAlMagatzem() = " + magatzemCombustible_v1.numContenidorsAlMagatzem() +  ", " + new String(magatzemCombustible_v1.posicionsEnMagatzem));

                i++;
            }
        }
    }
}
