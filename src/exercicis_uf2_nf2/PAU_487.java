package exercicis_uf2_nf2;

import Pantalles.MenuConstructorPantalla;
import semafors_1_sync_and_semaphores.Exercici_2_v4;
import semafors_2_only_semaphores.Exercici_2_v5;
import semafors_3_sync_and_semaphores.Exercici_2_v6;
//import com.db4o.Db4oEmbedded;
//import com.db4o.ObjectContainer;
//import com.db4o.config.EmbeddedConfiguration;

import java.io.IOException;
import java.util.Scanner;


/**
 *
 * @author gmartinez
 */
public class PAU_487 {
    /*
    SISTEMA DE NAVEGACIÓ BASAT EN WAYPOINTS.
    ES DONEN D'ALTA DIVERSOS WAYPOINTS DE L'ESPAI (ORBITA MARCIANA, PUNT LAGRANGE TERRA-LLUNA, PHOBOS, SATURN, LLUNA,...).
    ES PODEN MEMORITZAR DIVERSES RUTES AFEGINT DIVERSOS WAYPOINTS A CADA RUTA.

    */
    public static void bloquejarPantalla() {
        Scanner in = new Scanner(System.in);
        System.out.print("\nToca 'C' per a continuar ");
        while (in.hasNext()) {
            if ("C".equalsIgnoreCase(in.next())) break;
        }
    }


    public static void menuPAU_487() throws IOException  {

        String opcio;
        Scanner sc = new Scanner(System.in);
        StringBuilder menu = new StringBuilder("");


        do {
            menu.delete(0, menu.length());

            menu.append(System.getProperty("line.separator"));
            menu.append("PAU-487 ");
            menu.append(System.getProperty("line.separator"));
            menu.append(System.getProperty("line.separator"));
            menu.append("1. Threads implements Runnable");
            menu.append(System.getProperty("line.separator"));
            menu.append("2. Threads extends Thread (I)");
            menu.append(System.getProperty("line.separator"));
            menu.append("3. Threads extends Thread (II)");
            menu.append(System.getProperty("line.separator"));
            menu.append("4. Threads amb wait() i notify()");
            menu.append(System.getProperty("line.separator"));
            menu.append("5. Threads amb semàfors (I)");
            menu.append(System.getProperty("line.separator"));
            menu.append("6. Threads amb semàfors (II)");
            menu.append(System.getProperty("line.separator"));
            menu.append(System.getProperty("line.separator"));
            menu.append("10. LLançar tots els torpedes");
            menu.append(System.getProperty("line.separator"));
            menu.append(System.getProperty("line.separator"));
            menu.append("50. Tornar al menú pare (PNS-24 Puma)");
            menu.append(System.getProperty("line.separator"));

            System.out.print(MenuConstructorPantalla.constructorPantalla(menu));

            opcio = sc.next();

            switch (opcio) {
                case "1":
                    Exercici_1.inicialitzarPrograma();
                    bloquejarPantalla();
                    break;
                case "2":
                    Exercici_2_v1.inicialitzarPrograma();
                    bloquejarPantalla();
                    break;
                case "3":
                    Exercici_2_v2.inicialitzarPrograma();
                    bloquejarPantalla();
                    break;
                case "4":
                    Exercici_2_v3.inicialitzarPrograma();
                    bloquejarPantalla();
                    break;
                case "5":
                    Exercici_2_v4.inicialitzarPrograma();
                    bloquejarPantalla();
                    break;
                case "6":
                    Exercici_2_v5.inicialitzarPrograma();
                    bloquejarPantalla();
                    break;
                case "10":
                    Exercici_2_v6.inicialitzarPrograma();
                    bloquejarPantalla();
                    break;
                case "50":
                    break;
                default:
                    System.out.println("COMANDA NO RECONEGUDA");
            }
        } while (!opcio.equals("50"));
    }
}
