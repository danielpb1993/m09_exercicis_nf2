package exercicis_uf2_nf2;

public class DeptEnginyeriaConsumidor_v3 implements Runnable{
    MagatzemCombustible_v3 magatzemCombustible_v3 = new MagatzemCombustible_v3();

    public DeptEnginyeriaConsumidor_v3(MagatzemCombustible_v3 magatzemCombustible_v3) {
        this.magatzemCombustible_v3 = magatzemCombustible_v3;
    }

    @Override
    public void run() {
//        char contenidor;
        int i = 0;

        System.out.println("2222 - DeptEnginyeriaConsumidor.INICI");

        i = 0;
        while ( i < 13) {
            System.out.println("2222.1 - [" + i + "] - DeptEnginyeriaConsumidor.run(), numContenidorsAlMagatzem() = " + magatzemCombustible_v3.numContenidorsAlMagatzem() + ", " + magatzemCombustible_v3.posicionsEnMagatzem);
            magatzemCombustible_v3.consumirContenidorDeCombustible();
            System.out.println("2222.2 - [" + i + "] - DeptEnginyeriaConsumidor.run(), numContenidorsAlMagatzem() = " + magatzemCombustible_v3.numContenidorsAlMagatzem() + ", " + magatzemCombustible_v3.posicionsEnMagatzem);

            i++;
        }

        System.out.println("2222 - DeptEnginyeriaConsumidor.FI");
    }

}
