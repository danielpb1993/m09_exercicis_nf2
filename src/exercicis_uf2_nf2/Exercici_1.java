package exercicis_uf2_nf2;

public class Exercici_1 {
    static long idThread;

    public static void inicialitzarPrograma() {

        Magatzem magatzemTmp = new Magatzem();

        System.out.println("Exercici_1.inicialitzarPrograma() - INICI");

        CapDeDepartament objCapDeDepartamentComandament = new CapDeDepartament(magatzemTmp, 100);
        CapDeDepartament ojbCapDepartamentArmes = new CapDeDepartament(magatzemTmp, 100);
        CapDeDepartament ojbCapDepartamentTimoINavegacio = new CapDeDepartament(magatzemTmp, 100);
        CapDeDepartament ojbCapDepartamentEnginyeria = new CapDeDepartament(magatzemTmp, 100);
        CapDeDepartament ojbCapDepartamentCiencia = new CapDeDepartament(magatzemTmp, 100);

        Thread filCapDepartamentComandament = new Thread(objCapDeDepartamentComandament);
        filCapDepartamentComandament.setName("Fil del departament 'comandament'");
        idThread = filCapDepartamentComandament.getId();

        Thread filCapDepartamentArmes = new Thread(ojbCapDepartamentArmes);
        filCapDepartamentArmes.setName("Fil del departament 'armes'");

        Thread filCapDepartamentTimoINavegacio = new Thread(ojbCapDepartamentTimoINavegacio);
        filCapDepartamentTimoINavegacio.setName("Fil del departament 'timó i navegació'");

        Thread filCapDepartamentEnginyeria = new Thread(ojbCapDepartamentEnginyeria);
        filCapDepartamentEnginyeria.setName("Fil del departament 'enginyeria'");

        Thread filCapDepartamentCiencia = new Thread(ojbCapDepartamentCiencia);
        filCapDepartamentCiencia.setName("Fil del departament 'ciència'");

        filCapDepartamentComandament.start();
        filCapDepartamentArmes.start();
        filCapDepartamentTimoINavegacio.start();
        filCapDepartamentEnginyeria.start();
        filCapDepartamentCiencia.start();

        System.out.println("Exercici_1 magatzem.comprovarQuantitatRacions() = " + magatzemTmp.comprovarQuantitatRacions());
        System.out.println("Exercici_1.inicialitzarPrograma() - FI");
    }
}
