package exercicis_uf2_nf2;

public class MagatzemCombustible_v1 {
    char[] posicionsEnMagatzem = {'0','0','0','0','0','0','0','0','0','0'};
    int posicio = -1;

    public MagatzemCombustible_v1() {
    }

    public synchronized int numContenidorsAlMagatzem() {
        int numContenidors;
        numContenidors = posicio + 1;
        return numContenidors;
    }

    public void produirContenidorDeCombustible() {
        if ((-1 <= posicio) && (posicio <= 8)){		//De  [-1,8] --> [0,9].
            posicio++;

            posicionsEnMagatzem[posicio] = '1';
        }
    }

    public void consumirContenidorDeCombustible() {
        if ((0 <= posicio) && (posicio <= 9)){		// [0,9].
            posicionsEnMagatzem[posicio] = '0';
            posicio--;
        }
    }
}
