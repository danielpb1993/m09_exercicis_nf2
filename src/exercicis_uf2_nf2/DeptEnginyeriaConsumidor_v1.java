package exercicis_uf2_nf2;

public class DeptEnginyeriaConsumidor_v1 extends Thread {
    private MagatzemCombustible_v1 magatzemCombustible_v1;

    public DeptEnginyeriaConsumidor_v1(MagatzemCombustible_v1 magatzemCombustible_v1) {
        this.magatzemCombustible_v1 = magatzemCombustible_v1;
    }

    @Override
    public void run() {
        int i = 0;

        System.out.println("2222 - DeptEnginyeriaConsumidor.INICI");

        while (i < 13) {
            if (magatzemCombustible_v1.numContenidorsAlMagatzem() > 0) {
                System.out.println("2222.1 - [" + i + "] - DeptEnginyeriaConsumidor.run(), numContenidorsAlMagatzem() = " + magatzemCombustible_v1.numContenidorsAlMagatzem() + ", " + new String(magatzemCombustible_v1.posicionsEnMagatzem));
                magatzemCombustible_v1.consumirContenidorDeCombustible();
                System.out.println("2222.2 - [" + i + "] - DeptEnginyeriaConsumidor.run(), numContenidorsAlMagatzem() = " + magatzemCombustible_v1.numContenidorsAlMagatzem() +  ", " + new String(magatzemCombustible_v1.posicionsEnMagatzem));

                i++;
            }
            System.out.println("2222 - DeptEnginyeriaConsumidor.FI");
        }
    }
}
