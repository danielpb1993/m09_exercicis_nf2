package semafors_1_sync_and_semaphores;

public class DeptEnginyeriaConsumidor_v4 implements Runnable {
    MagatzemCombustible_v4 magatzemCombustible_v4;

    public DeptEnginyeriaConsumidor_v4(MagatzemCombustible_v4 magatzemCombustible_v4) {
        this.magatzemCombustible_v4 = magatzemCombustible_v4;
    }

    @Override
    public void run() {
        int i = 0;
        boolean exitOperacio;
        System.out.println("2222 - DeptEnginyeriaConsumidor.INICI");
        while (i < 13) {
            exitOperacio = magatzemCombustible_v4.consumirContenidorDeCombustible();
            if (exitOperacio == true) {
                i++;
            }
        }
        System.out.println("2222 - DeptEnginyeriaConsumidor.FI");
    }
}
