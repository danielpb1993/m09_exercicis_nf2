package semafors_1_sync_and_semaphores;

public class DeptCienciaProductor_v4 implements Runnable {
    MagatzemCombustible_v4 magatzemCombustible_v4;

    public DeptCienciaProductor_v4(MagatzemCombustible_v4 magatzemCombustible_v4) {
        this.magatzemCombustible_v4 = magatzemCombustible_v4;
    }

    @Override
    public void run() {
        int i;
        boolean exitOperacio;

        System.out.println("1111 - DeptCienciaProductor.INICI");

        i = 0;
        while (i < 20) {
            exitOperacio = magatzemCombustible_v4.produirContenidorDeCombustible();
            if (exitOperacio == true) {
                i++;
            }
        }
        System.out.println("1111 - DeptCienciaProductor.FI");
    }
}
